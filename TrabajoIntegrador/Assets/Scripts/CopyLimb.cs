using UnityEngine;

public class CopyLimb : MonoBehaviour
{
    [SerializeField] private Transform targetLimb;
    private HingeJoint hj;
    private Quaternion targetInitialRotation;

    private void Start()
    {
        hj = GetComponent<HingeJoint>();
        targetInitialRotation = targetLimb.localRotation;
    }

    private void FixedUpdate()
    {
        CopyRotation();
    }

    private void CopyRotation()
    {
           Quaternion desiredRotation = Quaternion.Inverse(targetLimb.localRotation) * targetInitialRotation;
            Vector3 desiredEulerAngles = desiredRotation.eulerAngles;
            Vector3 currentEulerAngles = hj.transform.rotation.eulerAngles;
            Vector3 rotationDifference = desiredEulerAngles - currentEulerAngles;

            hj.transform.Rotate(rotationDifference, Space.Self);
        

    }
}
