using UnityEngine;

public class Jugador : MonoBehaviour
{
    //public HingeJoint piernaIzquierdaJoint;
    //public HingeJoint piernaDerechaJoint;
    //public HingeJoint brazoIzquierdoJoint;
    //public HingeJoint brazoDerechoJoint;

    float MovX;
    float MovY;
    public float velocidadMovimiento = 5f;
    public float velRotacion = 200.0f;
    public float fuerzaSalto = 500f;
    public float gravedad = 10f;
    public float fuerzaRotacion = 100f;

    private Rigidbody rb;
    private bool saltando;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        MovX = 0;
        MovY = 0;
    }

    private void Update()
    {
        rb.velocity = new Vector3(MovX * velocidadMovimiento * Time.deltaTime, MovY * velocidadMovimiento * Time.deltaTime);
        if (Input.GetKeyDown(KeyCode.W))
        {
            MovY = 1;
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            MovY = -1;
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            MovX = -1;
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            MovX = 1;
        }

        if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S))
        {
            MovY = 0;
        }
        if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
        {
            MovX = 0;
        }

        transform.Rotate(0, MovX * Time.deltaTime * velRotacion, 0);
        transform.Translate(0, 0, MovY * Time.deltaTime * velocidadMovimiento);

        //float rotateY = 0f;
        //if (x != 0f || z != 0f)
        //{
        //    rotateY = Mathf.Atan2(x,z ) * Mathf.Rad2Deg;
        //}
        //transform.rotation = Quaternion.Euler(0f, rotateY, 0f);

        /*<<<<<<<<<<<<<<< SCRIPT SALTO >>>>>>>>>>>>>>>>>*/

        if (Input.GetKeyDown(KeyCode.Space) && !saltando)
        {
            JumpAction();
        }


    }

    private void JumpAction()
    {
        rb.AddForce(Vector3.up * fuerzaSalto, ForceMode.Impulse);
        saltando = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Piso"))
        {
            saltando = false;
        }
    }

    private void FixedUpdate()
    {
        AplicarGravedad();
    }

    private void AplicarGravedad()
    {
        rb.AddForce(Vector3.down * gravedad, ForceMode.Acceleration);
    }
}


