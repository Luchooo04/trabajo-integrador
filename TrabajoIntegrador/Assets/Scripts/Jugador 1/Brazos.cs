using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brazos : MonoBehaviour
{
    public float velocidadMovimiento = 5f;
    public float velocidadRotacion = 200f;

    private Rigidbody rb;
    private bool moverBrazos = false;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        float movimientoVertical = 0f;
        float movimientoHorizontal = Input.GetAxis("Horizontal");

        if (Input.GetKey(KeyCode.Q))
        {
            movimientoVertical = 1f;
            moverBrazos = true;
        }
        else if (Input.GetKeyUp(KeyCode.Q))
        {
            moverBrazos = false;
        }

        if (moverBrazos)
        {
            // Mover los brazos hacia adelante
            Vector3 movement = transform.forward * movimientoVertical * velocidadMovimiento * Time.deltaTime;
            rb.MovePosition(rb.position + movement);
        }

        // Rotar los brazos alrededor del eje vertical
        Quaternion rotacion = Quaternion.Euler(0f, movimientoHorizontal * velocidadRotacion * Time.deltaTime, 0f);
        rb.MoveRotation(rb.rotation * rotacion);
    }
}
