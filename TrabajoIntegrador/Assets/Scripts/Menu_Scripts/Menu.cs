using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void EmpezarNivel(string nombreNivel) 
    {
        SceneManager.LoadScene(nombreNivel);
    }

    public void Salir() 
    {
        Application.Quit();
        Debug.Log("cerrando");
    }
}
