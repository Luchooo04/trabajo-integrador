using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
  

    public float gameDuration = 180f; // Duraci�n del juego en segundos (3 minutos)
    private float currentTime = 0f;
    private bool isGameOver = false;

    public int player1Score = 0;
    public int player2Score = 0;

    public TextMeshProUGUI player1ScoreText;
    public TextMeshProUGUI player2ScoreText;
    public TextMeshProUGUI GanadorText;

    private void Start()
    {
        currentTime = 0f;
        isGameOver = false;
        UpdateScoreText();
    }

    private void Update()
    {
        if (!isGameOver)
        {
            currentTime += Time.deltaTime;

            if (currentTime >= gameDuration)
            {
                EndGame();
            }
        }
    }

    private void EndGame()
    {
        // Realizar acciones de fin de juego
        isGameOver = true;
        Debug.Log("Juego terminado");

        // Calcular y mostrar el puntaje final
        int winner = GetWinner();
        string winnerName = (winner == 1) ? "Jugador 1" : "Jugador 2";
        int winnerScore = (winner == 1) ? player1Score : player2Score;

        Debug.Log("Puntaje final - Jugador 1: " + player1Score + " objetos, Jugador 2: " + player2Score + " objetos");
        Debug.Log("El ganador es " + winnerName + " con " + winnerScore + " cajas entregadas.");

        GanadorText.text = "Gano" + winnerName + " con " + winnerScore + " puntos ";
    }

    public void IncreasePlayerScore(int playerNumber, int amount)
    {
        if (playerNumber == 1)
        {
            player1Score += amount;
        }
        else if (playerNumber == 2)
        {
            player2Score += amount;
        }

        UpdateScoreText();
    }

    private int GetWinner()
    {
        if (player1Score > player2Score)
        {
            return 1;
        }
        else if (player2Score > player1Score)
        {
            return 2;
        }
        else
        {
            return 0; // Empate
        }
    }

    private void UpdateScoreText()
    {
        player1ScoreText.text = "" + player1Score.ToString();
        player2ScoreText.text = "" + player2Score.ToString();
    }

   
}
