using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jugador2 : MonoBehaviour
{
    public float fuerzaSalto = 500f;
    public float gravedad = 10f;
    public float velRotacion = 200.0f;
    public float Velocidad = 5f;
     float MovX;
     float MovY;

    private bool saltando;

    Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        MovX = 0;
        MovY = 0;
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector3(MovX * Velocidad * Time.deltaTime, MovY * Velocidad * Time.deltaTime);
        if (Input.GetKeyDown(KeyCode.UpArrow)) 
        { 
         MovY = 1;
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            MovY = -1;
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            MovX = -1;
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            MovX = 1;
        }

        if (Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.DownArrow)) 
        {
            MovY = 0;
        }
        if (Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow))
        {
            MovX = 0;
        }
        transform.Rotate(0, MovX * Time.deltaTime * velRotacion, 0);
        transform.Translate(0, 0, MovY * Time.deltaTime * Velocidad);

        if (Input.GetKeyDown(KeyCode.RightControl) && !saltando)
        {
            JumpAction();
        }

    }

    private void JumpAction()
    {
        rb.AddForce(Vector3.up * fuerzaSalto, ForceMode.Impulse);
        saltando = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Piso"))
        {
            saltando = false;
        }
    }

    private void FixedUpdate()
    {
        AplicarGravedad();
    }

    private void AplicarGravedad()
    {
        rb.AddForce(Vector3.down * gravedad, ForceMode.Acceleration);
    }
}
