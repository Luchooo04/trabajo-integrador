using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
  
public class Agarrar : MonoBehaviour
{
    public float fuerza = 4000;
    public Rigidbody rb;
    private SpringJoint sp;
    private bool agarrando = false;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!agarrando && collision.gameObject.CompareTag("Item"))
        {
            sp = gameObject.AddComponent<SpringJoint>();
            sp.connectedBody = collision.rigidbody;
            sp.spring = 12000;
            sp.breakForce = fuerza;
            agarrando = true;
        }
    }

    //private void OnCollisionExit(Collision collision)
    //{
    //    if (agarrando && collision.gameObject.CompareTag("Item") && Input.GetKey(KeyCode.LeftAlt))
    //    {
    //        Destroy(sp);
    //        agarrando = false;
    //    }
    //}

    private void OnJointBreak(float breakForce)
    {
        agarrando = false;
    }
}

    
   




