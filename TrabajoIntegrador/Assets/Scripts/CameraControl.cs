using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public float rotationSpeed = 1f;
    public Transform root;
    public HingeJoint hipJoint;
    public HingeJoint stomachJoint;

    float mouseX, mouseY;

    public float stomachOffset;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        CamControl();
    }

    void CamControl()
    {
        mouseX += Input.GetAxis("Mouse X") * rotationSpeed;
        mouseY -= Input.GetAxis("Mouse Y") * rotationSpeed;
        mouseY = Mathf.Clamp(mouseY, -35, 60);

        Quaternion rootRotation = Quaternion.Euler(0, mouseX, 0);
        Quaternion cameraRotation = Quaternion.Euler(-mouseY + stomachOffset, 0, 0);

        root.rotation = rootRotation;
        JointMotor hipMotor = hipJoint.motor;
        hipMotor.targetVelocity = -mouseX * rotationSpeed;
        hipJoint.motor = hipMotor;
        JointMotor stomachMotor = stomachJoint.motor;
        stomachMotor.targetVelocity = (-mouseY + stomachOffset) * rotationSpeed;
        stomachJoint.motor = stomachMotor;
    }
}
