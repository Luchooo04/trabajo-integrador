using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparador : MonoBehaviour
{
    public GameObject projectilePrefab;
    public Transform spawnPoint;
    public float fireInterval = 1f;
    public int maxProjectiles = 3;
    public Transform target; // Objeto para apuntar

    private List<GameObject> projectiles = new List<GameObject>();
    private bool isActive = false;
    private bool canFire = true;

    private void Start()
    {
        StartCoroutine(FireProjectiles());
    }

    private IEnumerator FireProjectiles()
    {
        while (true)
        {
            yield return new WaitForSeconds(fireInterval);

            if (isActive && projectiles.Count < maxProjectiles)
            {
                GameObject newProjectile = Instantiate(projectilePrefab, spawnPoint.position, spawnPoint.rotation);
                projectiles.Add(newProjectile);

                if (projectiles.Count > maxProjectiles)
                {
                    Destroy(projectiles[0]);
                    projectiles.RemoveAt(0);
                }

                // Apuntar hacia el objetivo
                Vector3 direction = target.position - spawnPoint.position;
                newProjectile.transform.rotation = Quaternion.LookRotation(direction);

                canFire = false;
            }
        }
    }

    private void Update()
    {
        if (isActive && !canFire && Input.GetKeyUp(KeyCode.Space))
        {
            canFire = false; // Habilitar el disparo cuando se suelta la tecla
        }
    }


    public void SetActive(bool active)
    {
        isActive = active;
    }
    public bool IsActive()
    {
        return isActive;
    }
}



