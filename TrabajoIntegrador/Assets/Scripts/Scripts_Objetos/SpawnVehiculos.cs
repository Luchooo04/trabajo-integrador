using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnVehiculos : MonoBehaviour
{
    public GameObject[] prefabOptions;
    public Transform spawnPoint;
    public Transform targetPoint;
    public float minSpawnInterval = 2f;
    public float maxSpawnInterval = 5f;
    public float movementSpeed = 5f;

    private void Start()
    {
        StartCoroutine(SpawnPrefabs());
    }

    private IEnumerator SpawnPrefabs()
    {
        while (true)
        {
            float randomSpawnInterval = Random.Range(minSpawnInterval, maxSpawnInterval);
            yield return new WaitForSeconds(randomSpawnInterval);

            int randomIndex = Random.Range(0, prefabOptions.Length);
            GameObject prefab = prefabOptions[randomIndex];

            GameObject spawnedPrefab = Instantiate(prefab, spawnPoint.position, Quaternion.LookRotation(targetPoint.position - spawnPoint.position));
            StartCoroutine(MovePrefab(spawnedPrefab));

            yield return null;
        }
    }

    private IEnumerator MovePrefab(GameObject prefab)
    {
        while (Vector3.Distance(prefab.transform.position, targetPoint.position) > 0.1f)
        {
            prefab.transform.position = Vector3.MoveTowards(prefab.transform.position, targetPoint.position, movementSpeed * Time.deltaTime);
            yield return null;
        }

        Destroy(prefab);
    }
}
