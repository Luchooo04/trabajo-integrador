using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proyectil : MonoBehaviour
{
    private Transform target;
    public float speed = 5f;

    public void SetTarget(Transform newTarget)
    {
        target = newTarget;
    }

    private void Update()
    {
        if (target != null)
        {
            // Realizar el movimiento del proyectil hacia el objetivo
            transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);

            // Si el proyectil llega al objetivo, realizar las acciones necesarias
            if (transform.position == target.position)
            {
                // Realizar acciones cuando el proyectil alcanza al objetivo
                // ...

                // Destruir el proyectil
                Destroy(gameObject);
            }
        }
        else
        {
            // Destruir el proyectil si no tiene un objetivo asignado
            Destroy(gameObject);
        }
    }
}
