using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torreta : MonoBehaviour
{
    public GameObject Proyectil;
    public Transform puntoInicio;
    public Transform target;
    public float intervaloFuego = 1f;
    public float ConteoInicio = 120f; // Tiempo en segundos para empezar a disparar
    private float tiempoActual = 0f;
    private bool disparando = false;

    private void Start()
    {
        tiempoActual = 0f;
    }

    private void Update()
    {
        tiempoActual += Time.deltaTime;

        if (tiempoActual >= ConteoInicio)
        {
            disparando = true;
        }

        if (disparando && tiempoActual >= ConteoInicio && tiempoActual % intervaloFuego < Time.deltaTime)
        {
            FireProjectile();
        }
    }

    private void FireProjectile()
    {
        // Calcular la direcci�n hacia el objetivo
        Vector3 direction = target.position - puntoInicio.position;
        Quaternion rotation = Quaternion.LookRotation(direction);

        // Instanciar el proyectil en la posici�n y rotaci�n del punto de disparo
        GameObject newProjectile = Instantiate(Proyectil, puntoInicio.position, rotation);

        // Asignar el objetivo al proyectil
        Proyectil proyectil = newProjectile.GetComponent<Proyectil>();
        if (proyectil != null)
        {
            proyectil.SetTarget(target);
        }
    }
}
