using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesaparecerObjeto : MonoBehaviour
{
    GameManager gameManager; // Referencia al GameManager

    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Item"))
        {
            // Incrementar el puntaje del jugador 1 en 1
            gameManager.IncreasePlayerScore(1, 1);
            // Destruir el objeto que llev� el jugador
            Destroy(other.gameObject);
        }
    }
}
