using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivarDisparador : MonoBehaviour
{
    public Disparador disparador;
    private bool playerInsideCollider = false;

    private void Update()
    {
        if (playerInsideCollider && Input.GetKeyDown(KeyCode.K))
        {
            bool activar = !disparador.IsActive();
            disparador.SetActive(activar);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerInsideCollider = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerInsideCollider = false;
        }
    }

    //private void OnGUI()
    //{
    //    if (playerInsideCollider)
    //    {
    //        GUI.Label(new Rect(10, 10, 200, 20), "Presiona �K� para activar/desactivar");
    //    }
    //}
}
